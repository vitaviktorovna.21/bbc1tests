using BBCTests.PageObjects;
using BBCTests.Support;

namespace BBCTests.StepDefinitions
{
    [Binding()]
    [Scope(Feature = "NewsFeature")]
    public sealed class NewsStepDefinitions : BaseSteps
    {
        private HomePageObject homePage;
        private NewsPageObject newsPage;
        private ResultSearchPageObject resultSearchPage;
        private SearchPageObject searchPage;

        [Given(@"User open Home page")]
        public void GivenUserOpenHomePage()
        {
            homePage = new HomePageObject(_webDriver);
        }

        
        [Given(@"User stores the text of the headline article and click on the Search bar")]
        public void GivenUserStoresTheTextOfTheHeadlineArticleAndClickOnTheSearchBar()
        {
            newsPage.SearchField.Click();
        }

        [Given(@"User enter the text on Search field and ckick Search button")]
        public void GivenUserEnterTheTextOnSearchFieldAndCkickSearchButton()
        {
            searchPage.SearchInput.SendKeys(TestSettings.RandomText);

            searchPage.SearchButton.Click();
        }

        [Given(@"User enter the text on Search field and click Search button")]
        public void GivenUserEnterTheTextOnSearchFieldAndClickSearchButton()
        {
            homePage.NewsButton.Click();
        }

        [When(@"User click on News tab")]
        public void WhenUserClickOnNewsTab()
        {
            homePage.NewsButton.Click();
        }

        [When(@"User should be navigated to Search page")]
        public void WhenUserShouldBeNavigatedToSearchPage()
        {
            searchPage = new SearchPageObject(_webDriver);
        }

        [When(@"User should be navigated to Results Search page")]
        public void WhenUserShouldBeNavigatedToResultsSearchPage()
        {
            resultSearchPage = new ResultSearchPageObject(_webDriver);  
        }

        [Then(@"User should be navigated to News page")]
        public void ThenUserShouldBeNavigatedToNewsPage()
        {
            newsPage = new NewsPageObject(_webDriver);
        }

        [Then(@"User see main news of the site in large format (.*)")]
        public void ThenUserSeeMainNewsOfTheSiteInLargeFormat(string expectedMessage)
        {
            var actualMessage = newsPage.MainNewsButton.Text;

            actualMessage.Should().Contain(expectedMessage);
        }

        [Then(@"User see secondary article titles within the visible window")]
        public void ThenUserSeeSecondaryArticleTitlesWithinTheVisibleWindow()
        {
            newsPage.ArticleTitlelList.Count.Should().Be(TestSettings.headlinesArticleArray.Length);

            newsPage.ArticleTitlelList.Select(article => article.Text).Should().Contain(TestSettings.headlinesArticleArray);
        }

        [Then(@"User see Search page")]
        public void ThenUserSeeSearchPage()
        {
            _webDriver.Url.Should().Contain(TestSettings.UrlText);
        }

        [Then(@"User Check the name of the first article")]
        public void ThenUserCheckTheNameOfTheFirstArticle()
        {
            resultSearchPage.FirstArticleField.Text.Should().Contain(TestSettings.RandomText);
        }

    }
}