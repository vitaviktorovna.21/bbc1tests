﻿using BBCTests.Support;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace BBCTests.StepDefinitions
{
    public class BaseSteps
    {
        protected IWebDriver _webDriver;

        [AfterScenario]
        protected void DoAfterEach()
        {
            _webDriver.Quit();
        }
        [BeforeScenario]
        protected void DoBeforeEach()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Manage().Cookies.DeleteAllCookies();
            _webDriver.Navigate().GoToUrl(TestSettings.HostPrefix);
            _webDriver.Manage().Window.Maximize();
        }
    }
}
