﻿using BBCTests.PageObjects;
using BBCTests.Support;

namespace BBCTests.StepDefinitions
{
    [Binding]
    [Scope(Feature = "YourStoryFormFeature")]
    public sealed class YourStoryFormStepDefinitions : BaseSteps
    {
        private CoronavirusPageObject coronavirusPage;
        private HomePageObject homePage;
        private NewsPageObject newsPage;
        private StoryPageObject storyPage;
        private GetIinTouchPageObject getIinTouchPageObject;

        [Given(@"User open Home page")]
        public void GivenUserOpenHomePage()
        {
            homePage = new HomePageObject(_webDriver);
        }

        [Given(@"User click on News tab and should be navigated to News page")]
        public void GivenUserClickOnNewsTabAndShouldBeNavigatedToNewsPage()
        {
            homePage.NewsButton.Click();
        }

        [Given(@"User click on Coronavirus tab and should be navigated to Coronavirus page")]
        public void GivenUserClickOnCoronavirusTabAndShouldBeNavigatedToCoronavirusPage()
        {
            newsPage = new NewsPageObject(_webDriver);
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            newsPage.CoronavirusButton.Click();
        }

        [Given(@"User enter your Contact number")]
        public void GivenUserEnterYourContactNumber()
        {
            getIinTouchPageObject = new GetIinTouchPageObject(_webDriver);
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            getIinTouchPageObject.ContactNumberInput.SendKeys(TestSettings.ContactNumber);
        }

        [Given(@"User enter your Location")]
        public void GivenUserEnterYourLocation()
        {
            getIinTouchPageObject.LokationInput.SendKeys(TestSettings.Location);
        }

        [Given(@"User enter your Age")]
        public void GivenUserEnterYourAge()
        {
            getIinTouchPageObject.AgeInput.SendKeys(TestSettings.Age);
        }

        [Given(@"User enter your Question")]
        public void GivenUserEnterYourQuestion()
        {
            getIinTouchPageObject.QuestionInput.SendKeys(TestSettings.Question);
        }

        [Given(@"User enter your Email")]
        public void GivenUserEnterYourEmail()
        {
            getIinTouchPageObject.EmailAdressInput.SendKeys(TestSettings.EmailAdress);
        }

        [Given(@"User enter your Name")]
        public void GivenUserEnterYourName()
        {
            getIinTouchPageObject.NameInput.SendKeys(TestSettings.Name);
        }

        [Given(@"User click I accept input")]
        public void GivenUserClickIAcceptInput()
        {
            getIinTouchPageObject.IAcceptCheckbox.Click();
        }

        [When(@"User click Your Coronavirus Stories button")]
        public void WhenUserClickYourCoronavirusStoriesButton()
        {
            coronavirusPage = new CoronavirusPageObject(_webDriver);
            coronavirusPage.YourQuestionButton.Click();
        }

        [When(@"User click Submit button")]
        public void WhenUserClickSubmitButton()
        {
            getIinTouchPageObject.SubmitButton.Click();
        }

        [Then(@"User should be navigated to Story page and see form your Question to BBC")]
        public void ThenUserShouldBeNavigatedToStoryPageAndSeeForm()
        {
            storyPage = new StoryPageObject(_webDriver);  
            storyPage.YourQuestionsButton.Click();
        }

        [Then(@"User see message about Error")]
        public void ThenUserSeeMessageAboutError()
        {
            getIinTouchPageObject.FormTab.Text.Should().Contain(TestSettings.Errormessage);
        }

        [Then(@"User see message about Error accept")]
        public void ThenUserSeeMessageAboutErrorAccept()
        {
            getIinTouchPageObject.FormTab.Text.Should().Contain(TestSettings.Erroraccepted);
        }
    }
}
