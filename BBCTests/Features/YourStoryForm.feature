﻿Feature: YourStoryFormFeature
The test checks the work of the form your Question to BBC

@mytag
Scenario: User should be able to find the form your Question to BBC
    Given User open Home page
    And User click on News tab and should be navigated to News page
    And User click on Coronavirus tab and should be navigated to Coronavirus page
    When User click Your Coronavirus Stories button  
    Then User should be navigated to Story page and see form your Question to BBC

Scenario: User fills out the form Your question to the BBC without entering an email and sees an error message
    Given User open Home page
    And User click on News tab and should be navigated to News page
    And User click on Coronavirus tab and should be navigated to Coronavirus page
    When User click Your Coronavirus Stories button  
    Then User should be navigated to Story page and see form your Question to BBC
    Given User enter your Contact number 
    And User enter your Location 
    And User enter your Age 
    And User enter your Question
    And User enter your Name
    And User click I accept input 
    When User click Submit button
    Then User see message about Error 

Scenario: User fills out the form Your question to the BBC without entering a name and sees an error message
    Given User open Home page
    And User click on News tab and should be navigated to News page
    And User click on Coronavirus tab and should be navigated to Coronavirus page
    When User click Your Coronavirus Stories button  
    Then User should be navigated to Story page and see form your Question to BBC
    Given User enter your Contact number 
    And User enter your Location 
    And User enter your Age 
    And User enter your Question
    And User enter your Email
    And User click I accept input 
    When User click Submit button
    Then User see message about Error 

Scenario: User fills out the form Your question to the BBC without entering a question and sees an error message
    Given User open Home page
    And User click on News tab and should be navigated to News page
    And User click on Coronavirus tab and should be navigated to Coronavirus page
    When User click Your Coronavirus Stories button  
    Then User should be navigated to Story page and see form your Question to BBC
    Given User enter your Contact number 
    And User enter your Location 
    And User enter your Age 
    And User enter your Name
    And User enter your Email
    And User click I accept input 
    When User click Submit button
    Then User see message about Error 

Scenario: User enter data into fields of the form your Question to BBC without accept and sees an error message
    Given User open Home page
    And User click on News tab and should be navigated to News page
    And User click on Coronavirus tab and should be navigated to Coronavirus page
    When User click Your Coronavirus Stories button  
    Then User should be navigated to Story page and see form your Question to BBC
    Given User enter your Contact number 
    And User enter your Location 
    And User enter your Age 
    And User enter your Question
    And User enter your Name
    And User enter your Email
    When User click Submit button
    Then User see message about Error accept