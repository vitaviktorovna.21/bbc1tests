﻿Feature: NewsFeature

The script describes testing the news page on the BBC website.

Scenario: User should be able to see the main news of the site in large format
    Given User open Home page
    When User click on News tab
    Then User should be navigated to News page
    And User see main news of the site in large format Ukraine claims fight back in key eastern city

Scenario: User should be able to see secondary article titles
    Given User open Home page
    When User click on News tab
    Then User should be navigated to News page
    And User see secondary article titles within the visible window 
 
 Scenario: User sees the correct title of the first article when searching by title
    When User click on News tab
    Then User should be navigated to News page
    Given User stores the text of the headline article and click on the Search bar
    When User should be navigated to Search page
    Then User see Search page
    Given User enter the text on Search field and click Search button
    Then User Check the name of the first article 