﻿namespace BBCTests.Support
{
    public static class TestSettings
    {
        public static string[] headlinesArticleArray { get; } = new[]
        {
            "Meet the baby as old as the war",
            "Shot dead by police while trying to get fuel",
            "Iran's Khamenei accuses 'enemy' of stirring protests",
            "Russian man accused of Litvinenko killing dies",
            "Palace concert to continue jubilee festivities"
        };

        public const string HostPrefix = "https://www.bbc.com/";
        public const string RandomText = "Russian man accused of Litvinenko killing dies";
        public const string Question = "Glory to Ukraine";
        public const string UrlText = "search";
        public const string Name = "Ivan";
        public const string EmailAdress = "Ivan@mail.ru";
        public const string ContactNumber = "+380000000000";
        public const string Location = "Ukraine";
        public const string Age = "30";
        public const string Errormessage = "can't be blank";
        public const string Erroraccepted = "must be accepted";
    }
}
