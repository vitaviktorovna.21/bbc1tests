﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCTests.PageObjects
{
    public class GetIinTouchPageObject : BasePage
    {
        public GetIinTouchPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//div[@class='embed-content-container']")]
        public IWebElement FormTab { get; set; }

        [FindsBy(How = How.XPath, Using = "//textarea[@placeholder='What questions would you like us to answer?']")]
        public IWebElement QuestionInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Name']")]
        public IWebElement NameInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Email address']")]
        public IWebElement EmailAdressInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Contact number']")]
        public IWebElement ContactNumberInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Location ']")]
        public IWebElement LokationInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Age']")]
        public IWebElement AgeInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@type='checkbox']")]
        public IWebElement IAcceptCheckbox { get; set; } 

        [FindsBy(How = How.XPath, Using = "//button[@class='button']")]
        public IWebElement SubmitButton { get; set; }
    }
}
