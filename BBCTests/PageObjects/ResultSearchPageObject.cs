﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCTests.PageObjects
{
    public class ResultSearchPageObject : BasePage
    {
        public ResultSearchPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='main-content']")]
        public IWebElement FirstArticleField { get; set; }
    }
}
