﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCTests.PageObjects
{
    public class HomePageObject : BasePage
    {
        public HomePageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.CssSelector, Using = "nav li.orb-nav-newsdotcom")]
        public IWebElement NewsButton { get; set; }
    }
}
