﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCTests.PageObjects
{
    public class NewsPageObject : BasePage
    {
        public NewsPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'gs-c-promo-body gs-u-display-none ')]")]
        public IWebElement MainNewsButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'gs-c-promo nw-c-promo gs-o-faux-block-link gs-u-pb gs-u-pb+@m nw-p-default gs-c-promo--inline gs-c-promo--stacked@m nw-u-w-auto gs-c')]")]
        public IList<IWebElement> ArticleTitlelList { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='orbit-search-button']")]
        public IWebElement SearchField { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='orb-modules']/header/div[2]/div/div[1]/nav/ul/li[3]")]
        public IWebElement CoronavirusButton { get; set; }
    }
}
