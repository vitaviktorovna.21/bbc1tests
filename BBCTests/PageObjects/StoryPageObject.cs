﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCTests.PageObjects
{
    public class StoryPageObject : BasePage
    {
        public StoryPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//a[@href='/news/52143212']")]
        public IWebElement YourQuestionsButton { get; set; }
    }
}

