﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BBCTests.PageObjects
{
    public class SearchPageObject : BasePage
    {
        public SearchPageObject(IWebDriver webDriver) : base(webDriver) { }

        [FindsBy(How = How.XPath, Using = "//*[@id='search-input']")]
        public IWebElement SearchInput { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@data-testid='test-search-submit']")]
        public IWebElement SearchButton { get; set; }
    }
}
